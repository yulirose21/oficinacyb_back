var sql = require('mssql');
var sqlconfig = require('../config/envconfig').database;

exports.addCliente = async (req) => {
    try {
        let pool = await sql.connect(sqlconfig);
        let result = await pool.request()
            .input('id_cliente', sql.NVARCHAR(20), req.id_cliente)
            .input('nombre', sql.NVARCHAR(50), req.nombre)
            .input('atv', sql.NVARCHAR(50), req.atv)
            .input('fecha_nacimiento', sql.NVARCHAR(11), req.fecha_nacimiento)
            .input('fecha_vencimiento', sql.NVARCHAR(11), req.fecha_vencimiento)
            .input('representante_legal', sql.Int, req.representante_legal)
            .input('estado', sql.Int, req.estado)
            .input('tipo', sql.Int, req.tipo)
            .input('email', sql.NVARCHAR(50), req.email)
            .input('correo_contra', sql.NVARCHAR(30), req.correo_contra)
            .input('numero', sql.NVARCHAR(20), req.numero)
            .input('nombre_fact', sql.NVARCHAR(20), req.nombre_fact)
            .input('usuario', sql.NVARCHAR(20), req.usuario)
            .input('facturador_contra', sql.NVARCHAR(20), req.facturador_contra)
            .output('success', sql.Bit, 0)
            .execute('setCliente');
        sql.close();
        return result;
    }
    catch (excepcion) {
        sql.close();
        throw excepcion;
    }
}

exports.getAllCliente = async (req) => {
    try {
        let pool = await sql.connect(sqlconfig);
        let result = await pool.request()
            .execute('getAllCliente');
        sql.close();
        return result;
    }
    catch (excepcion) {
        sql.close();
        throw excepcion;
    }
}

exports.updateCliente = async (req) => {
    try {
        let pool = await sql.connect(sqlconfig);
        let result = await pool.request()
            .input('id_cliente', sql.NVARCHAR(20), req.id_cliente)
            .input('nombre', sql.NVARCHAR(50), req.nombre)
            .input('atv', sql.NVARCHAR(50), req.atv)
            .input('fecha_nacimiento', sql.NVARCHAR(11), req.fecha_nacimiento)
            .input('fecha_vencimiento', sql.NVARCHAR(11), req.fecha_vencimiento)
            .input('representante_legal', sql.Int, req.representante_legal)
            .input('estado', sql.Int, req.estado)
            .input('email', sql.NVARCHAR(50), req.email)
            .input('correo_contra', sql.NVARCHAR(30), req.correo_contra)
            .input('numero', sql.NVARCHAR(20), req.numero)
            .input('nombre_fact', sql.NVARCHAR(20), req.nombre_fact)
            .input('usuario', sql.NVARCHAR(20), req.usuario)
            .input('facturador_contra', sql.NVARCHAR(20), req.facturador_contra)
            .output('success', sql.Bit, 0)
            .execute('updateCliente');
        sql.close();
        return result;
    }
    catch (excepcion) {
        sql.close();
        throw excepcion;
    }
}

exports.updateTiv = async (req) => {
    try {
        let pool = await sql.connect(sqlconfig);
        let result = await pool.request()
            .input('id_cliente', sql.NVARCHAR(20), req.id_cliente)
            .input('a', sql.NVARCHAR(3), req.a)
            .input('b', sql.NVARCHAR(3), req.b)
            .input('c', sql.NVARCHAR(3), req.c)
            .input('d', sql.NVARCHAR(3), req.d)
            .input('e', sql.NVARCHAR(3), req.e)
            .input('f', sql.NVARCHAR(3), req.f)
            .input('g', sql.NVARCHAR(3), req.g)
            .input('h', sql.NVARCHAR(3), req.h)
            .input('i', sql.NVARCHAR(3), req.i)
            .input('j', sql.NVARCHAR(3), req.j)
            .input('k', sql.NVARCHAR(3), req.k)
            .input('l', sql.NVARCHAR(3), req.l)
            .input('m', sql.NVARCHAR(3), req.m)
            .input('n', sql.NVARCHAR(3), req.n)
            .input('o', sql.NVARCHAR(3), req.o)
            .output('success', sql.Bit, 0)
            .execute('updateTiv');
        sql.close();
        return result;
    }
    catch (excepcion) {
        sql.close();
        throw excepcion;
    }
}

exports.getRepresentantes = async (req) => {
    try {
        let pool = await sql.connect(sqlconfig);
        let result = await pool.request()
            .execute('getRepresentantes');
        sql.close();
        return result;
    }
    catch (excepcion) {
        sql.close();
        throw excepcion;
    }
}

exports.getRepresentante = async (req) => {
    try {
        let pool = await sql.connect(sqlconfig);
        let result = await pool.request()
            .input('id_cliente', sql.NVARCHAR(20), req.id_cliente)
            .execute('getRepresentante');
        sql.close();
        return result;
    }
    catch (excepcion) {
        sql.close();
        throw excepcion;
    }
}