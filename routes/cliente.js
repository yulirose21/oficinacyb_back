var express = require('express');
var router = express.Router();
const ClienteController = require('../controllers/cliente');
var HttpStatus = require('http-status-codes');

router.post('/agregar', (req, res) => {
    ClienteController.addCliente(req.body)
        .then(result => {
            if (result.output.success){
                res.sendStatus(204);
            }
            else
                res.status(400).json({ error: 'Error al agregar el cliente' });
        })
        .catch(err => {
            res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: err.message });
        })
});

router.get('/', (req, res) => {
    ClienteController.getAllCliente(req.query)
        .then(result => {
            if (result.recordset != null){
                res.status(200).json({ clientes: result.recordset });
            }
            else
                res.status(400).json({ error: 'No se pudieron obtener los clientes' });
        })
        .catch(err => {
            res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: err.message });
        })
});

router.put('/actualizar', (req, res) => {
    ClienteController.updateCliente(req.body)
        .then(result => {
            if (result.output.success){
                res.sendStatus(204);
            }
            else
                res.status(400).json({ error: 'Error al modificar el cliente' });
        })
        .catch(err => {
            res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: err.message });
        })
});

router.put('/tiv', (req, res) => {
    ClienteController.updateTiv(req.body)
        .then(result => {
            if (result.output.success){
                res.sendStatus(204);
            }
            else
                res.status(400).json({ error: 'Error al modificar la TIV' });
        })
        .catch(err => {
            res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: err.message });
        })
});

router.get('/representantes', (req, res) => {
    ClienteController.getRepresentantes(req.query)
        .then(result => {
            if (result.recordset != null){
                res.status(200).json({ clientes: result.recordset });
            }
            else
                res.status(400).json({ error: 'No se pudieron obtener los representantes' });
        })
        .catch(err => {
            res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: err.message });
        })
});


router.get('/representante', (req, res) => {
    ClienteController.getRepresentante(req.body)
        .then(result => {
            if (result.recordset != null){
                res.status(200).json({ clientes: result.recordset });
            }
            else
                res.status(400).json({ error: 'No se pudieron obtener los representantes' });
        })
        .catch(err => {
            res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: err.message });
        })
});

module.exports = router;